'use strict';

const axios = require('axios');
const config = require('../../utils/config');
const logger = require('../../utils/logger');

const urlElastic = `${config.elasticUrl}/entities/_search`;

exports.processText = dataElastic =>
    /* eslint no-underscore-dangle: ["error", { "allow": ["_type", "_source"] }]*/
    axios
        .post(urlElastic, dataElastic, {
            headers: {
                'Content-Type': 'application/json'
            },
            auth: {
                username: config.elasticUser,
                password: config.elasticPassword
            }
        })
        .then((response) => {
            const entities = {};
            const { hits } = response.data.hits;
            const entititesCount = {};
            for (let i = 0; i < hits.length; i++) {
                let entity = hits[i]._type.slice(0, -1);
                if (entity in entities) {
                    entititesCount[entity] += 1;
                    entity = `${entity} ${entititesCount[entity]}`;
                    entities[entity] = hits[i]._source.name;
                } else {
                    entities[entity] = hits[i]._source.name;
                    entititesCount[entity] = 1;
                }
            }

            // temporary workaround to detect the brand if only model is mentioned
            if (('model' in entities) && !('brand' in entities)) {
                entities.brand = hits[0]._source.brand;
            }

            return entities;
        })
        .catch((error) => {
            logger.info(error);
        });
