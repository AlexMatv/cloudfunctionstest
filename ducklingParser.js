'use strict';

const axios = require('axios');
const querystring = require('querystring');
const circularJSON = require('json-stringify-safe');
const config = require('../../utils/config');

const logger = require('../../utils/logger');

const Translate = require('@google-cloud/translate');

const translate = new Translate({
    projectId: 'lyolikhk-test'
});

function formatResults(resultsDuckl) {
    const results = {};
    const dimCount = {};
    let keyDim = '';
    let valueVal = '';
    let json = circularJSON(resultsDuckl);
    json = JSON.parse(json);

    for (let i = 0; i < json.data.length; i++) {
        const result = json.data[i];
        const diment = result.dim;
        const val = result.value;
        switch (result.value.type) {
            default:
                keyDim = `${diment}`;
                valueVal = `${val.value}`;
                //logger.info(`DEFAULT TEST: ${keyDim} ${valueVal}`);
                break;
            case 'value':
                keyDim = `${diment}`;
                if (val.unit) {
                    keyDim = `${keyDim} (${val.unit})`;
                } else if (val.grain) {
                    keyDim = `${keyDim} (${val.grain})`;
                }
                valueVal = val.value;
                //logger.info(`VALUE TEST: ${keyDim} ${valueVal}`);
                break;
            case 'interval':
                keyDim = `interval (${diment})`;
                if (val.from.unit) {
                    valueVal = `${val.from.value} ${val.from.unit} - ${val.to.value} ${val.to.unit}`;
                } else {
                    valueVal = `${val.from.value} - ${val.to.value}`;
                }
                //logger.info(`INTERVAL TEST: ${keyDim}${valueVal}`);
                break;
        }

        if (keyDim !== 'ordinal' && keyDim !== 'number' && keyDim !== 'phone-number') {
            if (keyDim in results) {
                dimCount[keyDim] += 1;
                keyDim = `${keyDim} ${dimCount[keyDim]}`;
                results[keyDim] = valueVal;
            } else {
                results[keyDim] = valueVal;
                dimCount[keyDim] = 1;
            }
        }
    }
    return results;
}

exports.processText = textToParse =>
    translate
        .detect(textToParse.substr(textToParse.indexOf(' ') + 1))
        .then((results) => {
            let langDuckl = '';
            let detections = results[0];
            detections = Array.isArray(detections) ? detections : [detections];

            logger.info('Detections:');
            detections.forEach((detection) => {
                logger.info(`${detection.input} => ${detection.language}`);
                langDuckl = detection.language;
            });

            const data = querystring.stringify({
                text: textToParse,
                lang: langDuckl,
                tz: 'NL'
            });
            return axios.post(config.ducklingUrl, data, {
                auth: {
                    username: config.ducklingUser,
                    password: config.ducklingPassword
                }
            });
        })
        .then((response) => {
            const entities = formatResults(response);
            //const resStr = JSON.stringify(entities);
            //logger.info(`Duckling results: ${resStr}`);
            return entities;
        })
        .catch((err) => {
            logger.error('ERROR:', err.toString());
        });
