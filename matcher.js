'use strict';

//const logger = require('../../utils/logger');

exports.processText = (text) => {

    const entities = {};
    let textMod = text;
    const atLeast = 1;
    const numWords = 3;

    // RE pattern to select valid characters. Invalid characters are replaced with a whitespace
    const REallowedChars = /[^a-zA-Z0-9'-]+/g;

    //"keys[0] = null", a word boundary with length zero is empty
    const keys = [null];
    const results = [];
    const numericalResults = [];

    for (let i = 1; i <= numWords; i++) {
        keys.push({});
    }

    // Remove all irrelevant characters
    textMod = textMod.replace(REallowedChars, ' ').replace(/^\s+/, '').replace(/\s+$/, '');

    textMod = textMod.split(/\s+/);
    const textlen = textMod.length;
    for (let i = 0, j; i < textlen; i++) {
        let s = textMod[i];
        keys[1][s] = (keys[1][s] || 0) + 1;
        for (j = 2; j <= numWords; j++) {
            if (i + j <= textlen) {
                const t = textMod[(i + j) - 1];
                s += ` ${t}`;
                keys[j][s] = (keys[j][s] || 0) + 1;
            } else break;
        }
    }

    for (let k = 1; k <= numWords; k++) {
        const key = keys[k];
        let ngramms = [];
        ngramms = Object.keys(key);
        for (let i = 0; i < ngramms.length; i += 1) {
            if (i >= atLeast) results.push(ngramms[i]);
        }
    }

    //Combining the numerical n-grams (phone number candidates)
    for (let i = 0; i < results.length; i += 1) {
        if (results[i].match(/^[0-9]|\(|\+/)) {
            const num = results[i].replace(/ |\(|-|\)|\+/g, '');
            numericalResults.push(num);
        }
    }

    const postcodeNL = /^[1-9][0-9]{3} ?(?!sa|sd|ss)[a-z]{2}$/i;
    const phoneNumber = /^[0-9]{10,11}$/;

    for (let i = 0; i < results.length; i += 1) {
        if (results[i].match(postcodeNL)) {
            entities['postcode (nl)'] = results[i];
        }
    }

    for (let i = 0; i < numericalResults.length; i += 1) {
        if (numericalResults[i].match(phoneNumber)) {
            entities['phone number'] = numericalResults[i];
        }
    }
    return entities;
};
