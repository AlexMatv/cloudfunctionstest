/*eslint no-underscore-dangle: ["error", { "allow": ["_source", "_index", "_id"] }]*/

'use strict';

const axios = require('axios');
const FormData = require('form-data');
const logger = require('../../utils/logger');
const fetch = require('node-fetch');
const config = require('../../utils/config');

const async = require('async');

module.exports = (event, callback) => {
    const pubsubMessage = event.data;
    const dataStr = pubsubMessage.data ? Buffer.from(pubsubMessage.data, 'base64').toString() : '{}';
    let file;
    try {
        file = JSON.parse(dataStr);
    } catch (err) {
        logger.error(`Filer | Error while parsing ${dataStr}`, err);
        callback();
        return;
    }
    logger.info(`Processing: ${file.bucket}/${file.name}`);

    const storage = require('@google-cloud/storage')({ projectId: 'lyolikhl-test' });
    const formData = new FormData();

    if (file.resourceState === 'not_exists') {
        logger.info(`File ${file.name} deleted.`);
        callback();
    } else if (file.metageneration === '1') {
        storage.bucket(file.bucket).file(file.name).download()
            .then(function (data) {
                formData.append('image', data[0]);

                return axios.post(config.tikaUrl, formData, {
                    headers: { 'content-type': 'multipart/form-data', 'Accept': 'text/plain' },
                    auth: {
                        username: config.tikaUser,
                        password: config.tikaPassword
                    },
                    responseType: 'text'
                });
            })
            .then((response) => {
                const fileSearchUrl = `${config.elasticUrl}/files/_search`;
                const query = {
                    query : {
                        constant_score : {
                            filter : {
                                term : {
                                    path : file.name
                                }
                            }
                        }
                    }
                };
                const options = {
                    method: 'POST',
                    body : JSON.stringify(query),
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': config.elasticAuth
                    }
                };

                //  console.log(JSON.stringify(options));
                fetch(fileSearchUrl, options)
                    .then((res) => {
                        const json = res.json();
                        if (json) {
                            return json;
                        }
                        logger.error(`Error getting ES data for ${file.name} : ${res.text}`);
                        return { total : 0, hits : {} };

                    })
                    .then((json) => {
                        const { total, hits: results } = json.hits;
                        if (total > 0) {
                            if (total > 1) {
                                logger.warn(`Multiple records found for file ${file.name} `);
                            }
                            async.map(results, (result) => {
                                const data = Object.assign({
                                    text: response.data
                                }, result._source);
                                logger.info(`Updating text of: ${data.title}(${file.name}): ${result._id}`);

                                const elasticSearchUrl = config.elasticUrl.concat(`/files/file/${result._id}`);
                                return axios.post(elasticSearchUrl, data, {
                                    headers: {
                                        'Content-Type': 'application/json'
                                    },
                                    auth: {
                                        username: config.elasticUser,
                                        password: config.elasticPassword
                                    }
                                }).then((resp) => {
                                    logger.info(`File ${file.name}|${result._id} updated: ${resp.statusText}`);
                                }).catch((error) => {
                                    logger.error(`Failed to update ${file.name}|${result._id}: ${error}`);
                                });
                            }, (err, values) => {
                                if (err !== 'null') {
                                    callback();
                                } else {
                                    callback(err);
                                }
                            });
                        } else {
                            logger.error(`No ES record found for file ${file.name} `);
                            callback(`No ES record found for file ${file.name} `);
                        }
                    });
            })
            .catch((error) => {
                logger.error(error);
                callback(error);
            });
    } else {
        logger.warn(`${file.bucket}/${file.name} Metageneration not requested`);
        callback();
    }
};
